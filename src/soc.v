
module soc(
	iFPGA_clock
);

	/* Clock Signal */
	input iFPGA_clock;    // Clock input (from FPGA)
	
	wire [31:0] PC;
	wire [23:0] CPU_Instruction;
	
	/////////////////////// Other parameters
	wire [15:0] VGA_Address; // Memory address used by the VGA to read/write
	wire [7:0] bgr_data_raw; // Output from the memory (color of the pixel)

	/////////////////////// Processor parameters
	wire [12:0] PROC_Address;  // Memory address used by the Processor to read/write 
	wire [63:0] PROC_DATA;     // Value to write on memory by the Processor
	wire PROC_wre;             // Write enable from the Processor
	wire [63:0] PROC_q;        // Memory value after read by the Processor

	///// vga clock constasnts
	wire VGA_CTRL_CLK;  // VGA clock output from the VGA_Audio: 25 MHz
	wire AUD_CTRL_CLK;  // AUD clock output from the VGA_Audio: 18 MHz
	wire mVGA_CLK;      // Second VGA clock output from the VGA_Audio: 25MHz
	
	Instruction_Memory instruction_memory (
		.PC(PC),
		.CPU_Instruction(CPU_Instruction)
	);
	
	cpu cpu_instance(
	.clock(iFPGA_clock),
	.pc(PC),
	.cpu_instruction(CPU_Instruction),
	.PROC_Address(PROC_Address),
	.PROC_DATA(PROC_DATA),
	.PROC_wre(PROC_wre),
	.PROC_q(PROC_q)
);
	
	/*memory	img_data_inst (
		.address_a(PROC_Address),     // Memory address used by the Processor to read/write 
		.address_b(VGA_Address),      // Memory address used by the VGA to read/write
		.clock_a(iFPGA_clock),        // Clock used when the memory is read by the Processor
		.clock_b(VGA_CLK),            // Clock used when the memory is read by the VGA controller
		.data_a(PROC_DATA),           // Value to write on memory by the Processor
		.data_b(0),                   // Value to write on memory by the VGA. //**Never used
		.wren_a(PROC_wre),            // Write enabled bit for processor. 1 indicates to write in that memory address
		.wren_b(0),                   // Write enabled bit for VGA. //**Never used
		.q_a(PROC_q),                 // Data bus output for the processor (64-bits)
		.q_b(bgr_data_raw)            // Data bus  output for the VGA (8-bits)
	);*/

endmodule
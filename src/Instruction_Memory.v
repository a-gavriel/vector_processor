module Instruction_Memory
(
  PC,
  CPU_Instruction
);

	input [31:0] PC;
	output reg [23:0] CPU_Instruction;	


	reg [23:0] Data[63:0];

	initial begin
  
		$readmemb("instruction_memory.mem", Data);

	end

	always @(PC) begin
		CPU_Instruction = Data[PC];
  end
  
endmodule
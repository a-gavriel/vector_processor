module cpu
(
	clock,
	pc,
	cpu_instruction
	
);
	
	// Module IO
	
	input clock;
	output reg[31:0] pc;
	input [23:0] cpu_instruction;
	
	// Auxiliar for initialization
	integer x_iterator;
	integer y_iterator;
	
	// Vectorial registers
	reg [7:0][7:0] vectorial_registers[7:0];
	
	
	// Pipeline registers
	// Fetch
	reg[23:0] fetch_instruction;
	
	
	// Decode
	reg[23:0] decode_instruction;
	reg[2:0] decode_instruction_type;
	reg[3:0] decode_operation_type;
	reg[2:0] decode_out;
	reg[2:0] decode_operator_1;
	reg[10:0] decode_operator_2;

	
	// Execute
	
	
	

	// First Run
	initial begin
		pc = -1;
		for (x_iterator = 0; x_iterator < 8; x_iterator = x_iterator + 1) begin
			for (y_iterator = 0; y_iterator < 8; y_iterator = y_iterator + 1) begin
				vectorial_registers[x_iterator][y_iterator] = 0;
			end
		end
	end
  
  // Always clock changes
	always @(clock) begin
	
		/* stage Fetch */
		
		// Store the current instruction in the Fetch register
		fetch_instruction <= cpu_instruction;
		
		pc <= pc + 1;
		
		
		/* Decode */
		decode_instruction <= fetch_instruction;
		decode_instruction_type <= fetch_instruction[23:21];
		decode_operation_type <= fetch_instruction[20:17];
		decode_out <= fetch_instruction[16:14];
		decode_operator_1 <= fetch_instruction[16:14];
		decode_operator_2 <= fetch_instruction[10:00];
		
		/* Execute */
		
		
	end
  
  
endmodule
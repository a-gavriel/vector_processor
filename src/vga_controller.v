module vga_controller(
    iRST_n,
    iVGA_CLK,
    ibgr_data_raw,
    oAddress,
    oBLANK_n,
    oHS,
    oVS,
    b_data,
    g_data,
    r_data
  );
  
  input iRST_n;
  input iVGA_CLK;
  input [7:0] ibgr_data_raw;
  output [15:0] oAddress;
  output reg oBLANK_n;
  output reg oHS;
  output reg oVS;
  output [7:0] b_data;
  output [7:0] g_data;  
  output [7:0] r_data;                        
  ///////// ////                     
  wire VGA_CLK_n;
  wire [7:0] index;
  wire cBLANK_n,cHS,cVS,rst;
  ////
  reg [15:0] Address;
  assign rst = ~iRST_n;

  video_sync_generator LTM_ins(
    .vga_clk(iVGA_CLK),
    .reset(rst),
    .blank_n(cBLANK_n),
    .HS(cHS),
    .VS(cVS)
  );
  ////
  //// Address generator
  always@(posedge iVGA_CLK,negedge iRST_n)
  begin
    if (!iRST_n)
      Address<=16'd0;
    else if (cHS==1'b0 && cVS==1'b0)
      Address<=16'd0;
    else if (cBLANK_n==1'b1)
      Address<=Address+1;
  end
  assign oAddress = Address;
  //////
  //////latch valid data at falling edge;
  assign b_data = ibgr_data_raw;
  assign g_data = ibgr_data_raw;
  assign r_data = ibgr_data_raw;
  ///////////////////
  //////Delay the iHD, iVD,iDEN for one clock cycle;
  always@(negedge iVGA_CLK)
  begin
    oHS<=cHS;
    oVS<=cVS;
    oBLANK_n<=cBLANK_n;
  end
  
endmodule

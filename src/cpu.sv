module cpu
(
	clock,
	pc,
	cpu_instruction,
	PROC_Address,
	PROC_DATA,
	PROC_wre,
	PROC_q	
);
	
	// Module IO
	
	input clock;
	output reg[31:0] pc;
	input [23:0] cpu_instruction;
	
	output reg [12:0] PROC_Address;  // Memory address used by the Processor to read/write 
	output reg [63:0] PROC_DATA;     // Value to write on memory by the Processor
	output reg PROC_wre;             // Write enable from the Processor
	input [63:0] PROC_q;        // Memory value after read by the Processor
	
	// Auxiliar for initialization
	integer x_iterator;
	integer y_iterator;
	
	// Vectorial registers
	reg [7:0][7:0] vectorial_registers[7:0];
	
	
	// Pipeline registers
	// Fetch
	reg[23:0] fetch_instruction;
	
	
	// Decode
	reg[23:0] decode_instruction;
	reg[2:0] decode_instruction_type;
	reg[3:0] decode_operation_type;
	reg[2:0] decode_out;
	reg[2:0] decode_operator_1;
	reg[10:0] decode_operator_2;

	
	// Execute
	reg[23:0] execute_instruction;
	reg[2:0] execute_instruction_type;
	reg[3:0] execute_operation_type;
	reg[2:0] execute_out;
	reg[2:0] execute_operator_1;
	reg[10:0] execute_operator_2;
	reg[13:0] execute_address;
	
	integer amount_of_lanes;
	
	
	
	

	// First Run
	initial begin
	
		pc = 0;
		for (x_iterator = 0; x_iterator < 8; x_iterator = x_iterator + 1) begin
			for (y_iterator = 0; y_iterator < 8; y_iterator = y_iterator + 1) begin
				vectorial_registers[x_iterator][y_iterator] = 8'b00001;
			end
		end
		
		/*for (amount_of_lanes = 0; amount_of_lanes < 8; amount_of_lanes = amount_of_lanes + 1) begin
						
			vectorial_registers
							[amount_of_lanes]
							[0] = amount_of_lanes;
		end*/
		
		
		
		
	end
  
  // Always clock changes
	always @(posedge clock) begin
	
		if (pc <= 4096) begin
			PROC_wre <= 1;
			PROC_DATA = 64'b0;
			PROC_Address = pc;
		end
		
	
		/* stage Fetch */
		
		// Store the current instruction in the Fetch register
		fetch_instruction <= cpu_instruction;
		
		pc <= pc + 1;
		
		
		/* Decode */
		decode_instruction <= fetch_instruction;
		decode_instruction_type <= fetch_instruction[23:21];
		decode_operation_type <= fetch_instruction[20:17];
		decode_out <= fetch_instruction[16:14];
		decode_operator_1 <= fetch_instruction[13:11];
		decode_operator_2 <= fetch_instruction[10:00];
		
		/* Execute */
		execute_instruction <= decode_instruction;
		execute_instruction_type <= decode_instruction[23:21];
		execute_operation_type <= decode_instruction[20:17];
		execute_out <= decode_instruction[16:14];
		execute_operator_1 <= decode_instruction[13:11];
		execute_operator_2 <= decode_instruction[10:00];
		
		if(decode_instruction_type == 3'b000) begin // Memory Vector Operation
			
			case (decode_operation_type)
				
				4'b0110 : begin // Load
				
					execute_address <= decode_instruction[13:0]; // Address to read
					PROC_Address <= decode_instruction[13:0]; // Set address to memory
					PROC_wre <= 0; // Flag to read
					
				end
				
				4'b0111 : begin // Store
					
					execute_address <= decode_instruction[13:0]; // Address to read
					PROC_Address <= decode_instruction[12:0]; // Set address to memory
					PROC_wre <= 1; // Flag to write
					
				end
			
			endcase
		
		end else if (decode_instruction_type == 3'b011) begin // Vector - Vector arithmetic
			
			case (decode_operation_type)
				
				4'b0000 : begin // Add
					
					// N/A See write-back due to combinational logic
					
				end
				
				4'b0001 : begin // Sub
					
					// N/A See write-back due to combinational logic
					
				end
			
			endcase
		
		end else if (decode_instruction_type == 3'b001) begin // Vector - vector logic
			
			case (decode_operation_type)
				
				4'b0010 : begin // NOT
					
					// N/A See write-back due to combinational logic
					
				end
				
				4'b0011 : begin // XOR
					
					// N/A See write-back due to combinational logic
					
				end
			
			endcase
		
		end
		
		
		/* Write-Back */
		
		if(execute_instruction_type == 3'b000) begin // Memory Vector Operation
			
			case (execute_operation_type)
				
				4'b0110 : begin // Load
					
					vectorial_registers[execute_operator_1] <= PROC_q;  
					
				end
				
				4'b0111 : begin // Store
					
					// N/A
					
				end
			
			endcase
		
		end else if (execute_instruction_type == 3'b011) begin // Vector - Vector arithmetic
			
			case (execute_operation_type)
				
				4'b0000 : begin // Add
					
					for (amount_of_lanes = 0; amount_of_lanes < 8; amount_of_lanes = amount_of_lanes + 1) begin
						
						vectorial_registers
							[ execute_instruction[16:14] ]
							[amount_of_lanes] <= vectorial_registers
														[ execute_instruction[13:11] ][amount_of_lanes] + 
													  vectorial_registers
														[ execute_instruction[2:0] ][amount_of_lanes];
					end
					
					
				end
				
				4'b0001 : begin // Sub
					
					
					for (amount_of_lanes = 0; amount_of_lanes < 8; amount_of_lanes = amount_of_lanes + 1) begin
						
						vectorial_registers
							[ execute_instruction[16:14] ]
							[amount_of_lanes] <= vectorial_registers
														[ execute_instruction[13:11] ][amount_of_lanes] - 
													  vectorial_registers
														[ execute_instruction[2:0] ][amount_of_lanes];
					end
					
				end
			
			endcase
		
		end else if (execute_instruction_type == 3'b001) begin // Vector - vector logic
			
			case (execute_operation_type)
				
				4'b0010 : begin // NOT
					
					for (amount_of_lanes = 0; amount_of_lanes < 8; amount_of_lanes = amount_of_lanes + 1) begin
						
						vectorial_registers
							[ execute_instruction[16:14] ]
							[amount_of_lanes] <= ~vectorial_registers
														[ execute_instruction[13:11] ][amount_of_lanes];
					end
					
				end
				
				4'b0011 : begin // XOR
					
					for (amount_of_lanes = 0; amount_of_lanes < 8; amount_of_lanes = amount_of_lanes + 1) begin
						
						vectorial_registers
							[ execute_instruction[16:14] ]
							[amount_of_lanes] <= vectorial_registers
														[ execute_instruction[13:11] ][amount_of_lanes] ^ 
															execute_instruction[10:0];
																
					end
					
				end
			
			endcase
		
		end
		
		
		
		
		
	end
  
  
endmodule
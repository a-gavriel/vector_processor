module cpu_tb ();

	/* Clock Signal */
	reg iFPGA_clock;
	
	/* Setup the clock */
	initial begin
		iFPGA_clock = 1'b0;
	end

	/* Toggle the clock */
	always begin
		#1 iFPGA_clock = ~iFPGA_clock;
	end

endmodule
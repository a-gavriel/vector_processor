import PIL  # needs the library Pillow
from PIL import Image, ImageOps
import numpy as np  # needs the library numpy
import matplotlib.pyplot as plt


#takes a chunk of 8 pixels and encrypts them using one of the following methods
def enc(vector, key, mode):  
  if mode == "1":    
    new = np.array([])
    key = [key[0]]*8    
    for i in range(len(vector)):
      value = vector[i] ^ key[i]      
      new =np.append(new,value)      
    return new
  if mode == "2":    
    new = np.array([])
    for i in range(len(vector)):
      value = (vector[i] + key[i] )%256
      new =np.append(new,value)      
    return new
  if mode == "3":    
    new = np.array([])
    k = key[0]
    for i in range(len(vector)):
      binary = bin(vector[i])[2:].zfill(8)
      value =  int(binary[k:] + binary[:k],2)
      new =np.append(new,value)      
    return new


#iterates through each row of the image and takes chunks of 8 pixels at a time to encrypt
def encript(imgE, key, mode):  
  jlen = len(imgE[0])      
  for i in range(len(imgE)):
    row = imgE[i]
    j = 0
    newline = np.array([])
    while j < jlen:
      vector = row[j:j+8]
      newVector = enc(vector,key,mode)
      newline = np.append(newline,newVector)
      j = j+8          
    imgE[i] = newline
  return imgE

# Read file
image = PIL.Image.open("../resources/cube.png")          # Opens the image using PIL
imageG = image.convert("L")           # Converts the image to greyscale

# Inverts the greyscale, since it's inverted by default

imgA = np.array(imageG)                 # Converts the image into a numpy matrix
imgB = imgA.reshape(256*256)          # Reshapes the image into a 1D array
imgFile = open("../resources/img.mif", "w+")       # Opens output file

### not used for generating the output
imgI = PIL.ImageOps.invert(imageG)    # Invert the image color for the grayscale
imgR = np.array(imgI)      #draws the inverted image (to be displayed in the fpga)

plt.figure(1)
plt.imshow(imgR,cmap=plt.cm.binary)

showEnc = input("encript? (0-1): ")
if showEnc == "1":
  keystring = (input("key (#,#,#,#,#,#,#,#): ")).split(",")
  key = []
  for i in keystring:
    key.append(int(i))#converts from string to int
  print("xor, sum, shift, swap")
  mode = input("mode (1-2-3-4): ")
  imgE = encript(imgR, key, mode)  
  
  plt.figure(2)
  plt.imshow(imgE,cmap=plt.cm.binary)
  plt.show()

# Adds the header of the file with metadata
start = """WIDTH = 8;
DEPTH = 65536;

ADDRESS_RADIX = HEX;
DATA_RADIX = HEX;

CONTENT BEGIN\n"""

data = start

# Iterates the image and for each pixel outputs a line with the format:
# pixelnumber:color;
# where pixelnumber and color are in hex
for i in range(256*256):
    counter = hex(i)[2:]
    value = hex(imgB[i])[2:]
    s = counter + ':' + value + ";\n"
    data += s

# Adds the tail of the metadata
data += "END;"

imgFile.write(data) # Write the data to the mif file
imgFile.close()     # Close the output file

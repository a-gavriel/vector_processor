module TestBench ();

	/* Clock Signal */
	reg iRST_n;         // Reset input (switch)
	reg iFPGA_clock;    // Clock input (from FPGA)
	wire VGA_CLK;       // VGA output (sent to the monitor)
	wire oBLANK_n;      // Blank output negated (where the monitor draws)
	wire oHS;           // Horizontal Sync output (horizontal pixel)
	wire oVS;           // Vertical Sync output (vertical pixel)
	wire [7:0] b_data;  // Blue color
	wire [7:0] g_data;  // Green color
	wire [7:0] r_data;  // Red color
	
	vector_processor vp (
		.iRST_n(iRST_n),
		.iFPGA_clock(iFPGA_clock),
		.oBLANK_n(oBLANK_n),
		.oHS(oHS),
		.oVS(oVS),
		.VGA_CLK(VGA_CLK),
		.b_data(b_data),
		.g_data(g_data),
		.r_data(r_data)
	);
	
	/* Setup the clock */
	initial begin
		iFPGA_clock = 1'b0;
		iRST_n = 1'b0;
		//#30 //$finish;
	end

	/* Toggle the clock */
	always begin
		#1 iFPGA_clock = ~iFPGA_clock;
	end

endmodule
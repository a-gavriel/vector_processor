module vector_processor(
    iRST_n,
    iFPGA_clock,
    oBLANK_n,
    oHS,
    oVS,
    VGA_CLK,
    b_data,
    g_data,
    r_data
  );

/////////////////////// I/O
input iRST_n;         // Reset input (switch)
input iFPGA_clock;    // Clock input (from FPGA)
output VGA_CLK;       // VGA output (sent to the monitor)
output oBLANK_n;      // Blank output negated (where the monitor draws)
output oHS;           // Horizontal Sync output (horizontal pixel)
output oVS;           // Vertical Sync output (vertical pixel)
output [7:0] b_data;  // Blue color
output [7:0] g_data;  // Green color
output [7:0] r_data;  // Red color
/////////////////////// Other parameters
wire [15:0] VGA_Address; // Memory address used by the VGA to read/write
wire [7:0] bgr_data_raw; // Output from the memory (color of the pixel)

/////////////////////// Processor parameters
wire [12:0] PROC_Address;  // Memory address used by the Processor to read/write 
wire [63:0] PROC_DATA;     // Value to write on memory by the Processor
wire PROC_wre;             // Write enable from the Processor
wire [63:0] PROC_q;        // Memory value after read by the Processor

///// vga clock constasnts
wire VGA_CTRL_CLK;  // VGA clock output from the VGA_Audio: 25 MHz
wire AUD_CTRL_CLK;  // AUD clock output from the VGA_Audio: 18 MHz
wire mVGA_CLK;      // Second VGA clock output from the VGA_Audio: 25MHz

VGA_Audio u1(
 .refclk(iFPGA_clock),    // refclk.clk
 .rst(~iRST_n),           // reset.reset
 .outclk_0(VGA_CTRL_CLK), // outclk0.clk
 .outclk_1(AUD_CTRL_CLK), // outclk1.clk
 .outclk_2(mVGA_CLK),     // outclk2.clk
 .locked()                // locked.export
);

// VGA clock used in the VGA (selected output from VGA_Audio)
assign VGA_CLK = VGA_CTRL_CLK; 

vga_controller vga_inst(
 .iRST_n(iRST_n),               // Reset input
 .iVGA_CLK(VGA_CLK),            // VGA clock input
 .ibgr_data_raw(bgr_data_raw),  // Data input to divide into RGB
 .oAddress(VGA_Address),        // Memory addres to obtain the color of the pixel
 .oBLANK_n(oBLANK_n),           // Blank output negated (where the monito draws)
 .oHS(oHS),                     // Horizontal sync output
 .oVS(oVS),                     // Vertical sync output
 .b_data(b_data),               // Blue color output
 .g_data(g_data),               // Green color output
 .r_data(r_data)                // Red color output
);

memory	img_data_inst (
  .address_a(PROC_Address),     // Memory address used by the Processor to read/write 
  .address_b(VGA_Address),      // Memory address used by the VGA to read/write
  .clock_a(iFPGA_clock),        // Clock used when the memory is read by the Processor
  .clock_b(VGA_CLK),            // Clock used when the memory is read by the VGA controller
  .data_a(PROC_DATA),           // Value to write on memory by the Processor
  .data_b(0),                   // Value to write on memory by the VGA. //**Never used
  .wren_a(PROC_wre),            // Write enabled bit for processor. 1 indicates to write in that memory address
  .wren_b(0),                   // Write enabled bit for VGA. //**Never used
  .q_a(PROC_q),                 // Data bus output for the processor (64-bits)
  .q_b(bgr_data_raw)            // Data bus  output for the VGA (8-bits)
);

cpu cpu_instance(
	.clock(iFPGA_clock),
	.pc(PC),
	.cpu_instruction(CPU_Instruction),
	.PROC_Address(PROC_Address),
	.PROC_DATA(PROC_DATA),
	.PROC_wre(PROC_wre),
	.PROC_q(PROC_q)
);



endmodule